New Media Design & Development I
================================

|Info|  |
|----|---|
|Olod|New Media Design & Development I|
|Auteur(s)|Mathias Notaert, Matthias De Lange, Ellen Tijtgat|
|Opleiding|Bachelor in de Grafische en digitale media|
|Academiejaar|2015-16|
|ECTS-fiche|https://bamaflexweb.arteveldehs.be/BMFUIDetailxOLOD.aspx?a=47532&b=5&c=1|

***

Documenten
----------


Auteurs
--------

**Mathias Notaert**

* <http://github.com/mathnota>
* <http://bitbucket.org/mathnota>
* <mathnota@student.arteveldehs.be>
	
**Matthias De Lange**

* <http://github.com/mattdela>
* <http://bitbucket.org/mattdela>
* <mattdela@student.arteveldehs.be>

**Ellen Tijtgat**

* <http://github.com/elletijt>
* <http://bitbucket.org/elletijt>
* <ellen.tijtgat@arteveldehs.be>

Arteveldehogeschool
-------------------

- <http://www.arteveldehogeschool.be>
- <http://www.arteveldehogeschool.be/ects>
- <http://www.arteveldehogeschool.be/bachelor-de-grafische-en-digitale-media>
- <http://twitter.com/bachelorGDM>
- <https://www.facebook.com/BachelorGrafischeEnDigitaleMedia?fref=ts>


Copyright and license
---------------------

> Deze cursus mag niet gebruikt worden in andere opleidingen, hogescholen en universiteiten wereldwijd. Indien dit het geval zou zijn zullen jullie juridische stappen ondergaan.
>
> Code en documentatie copyright 2003-2016 [Arteveldehogeschool](http://www.arteveldehogeschool.be) | Opleiding Bachelor in de grafische en digitale media | drdynscript. Cursusmateriaal, zoals code, documenten, ... uitgebracht onder de [MIT licentie](LICENSE).