/* TEAM */
Company: Arteveldehogeschool
Website: http://www.arteveldehs.be
Twitter: @ArteveldeHS
Email: info [at] arteveldehs [dot] be
Location: Ghent, Belgium

Data+Design+HTML+CSS+PHP+jQuery: Tijtgat - Ellen
Facebook: https://www.facebook.com/ellen.tijtgat
Location: Deinze, Belgium

Data+Design+HTML+CSS+PHP+jQuery: Notaert Mathias
Facebook: https://www.facebook.com/mathias.notaert
Location: Gooik, Belgium

Data+Design+HTML+CSS+PHP+jQuery: De Lange Matthias
Facebook: https://www.facebook.com/matthias.delange.5
Location: Deinze, Belgium


/* THANKS */
HTML5 Template: Boilerplate
Website: http://html5boilerplate.com/


/* SITE */
Last update: Always up-to-date
Standards: HTML5, CSS3
Components: Modernizr, jQuery, Google Maps
Software: PHP, MySQL, Sublime Text, Notepad++