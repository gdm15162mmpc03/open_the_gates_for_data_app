/*
 *	Title: Worldbank Application
 *	Modified: 19-11-2015
 *	Version: 1.0.0
 *	Author: Ellen Tijtgat , Matthias De Lange , Mathias Notaert
 * 	-----------------------------------------------
 */

// Anonymous function executed when document loaded
(function() {

    // Describe an App object with own functionalities
    var App = {
        init: function() {
            var self = this;

            //worldbank databank: countries
            this.WBCOUNTRIESAPIURL = "http://api.worldbank.org/countries/all?format=jsonP&prefix=jsonp_callback_{0}&per_page=300";

            //worldbank databank: forrest area
            this.WBFORRESTAREAPERCOUNTRYAPI = "http://api.worldbank.org/countries/{0}/indicators/AG.LND.FRST.ZS?format=jsonP&prefix=jsonp_callback_{1}&per_page=300";

            //worldbank databank: price diesel
            this.WBPRICEDIESELPERCOUNTRYAPI = "http://api.worldbank.org/countries/{0}/indicators/EP.PMP.DESL.CD?format=jsonP&prefix=jsonp_callback_{1}&per_page=300";

            //worldbank databank: price gasoline
            this.WBPRICEGASOLINEPERCOUNTRYAPI = "http://api.worldbank.org/countries/{0}/indicators/EP.PMP.SGAS.CD?format=jsonP&prefix=jsonp_callback_{1}&per_page=300";

            //worldbank databank: internet connections
            this.WBINTERNETCONNECTIONSPERCOUNTRYAPI = "http://api.worldbank.org/countries/{0}/indicators/EG.ELC.ACCS.ZS?format=jsonP&prefix=jsonp_callback_{1}&per_page=300";

            //worldbank databank: improved water sources
            this.WBIMPROVEDWATERSOURCESPERCOUNTRYAPI = "http://api.worldbank.org/countries/{0}/indicators/SH.H2O.SAFE.RU.ZS?format=jsonP&prefix=jsonp_callback_{1}&per_page=300";

            //worldbank databank: merchandise trade
            this.WBMERCHANDISETRADEPERCOUNTRYAPI = "http://api.worldbank.org/countries/{0}/indicators/TG.VAL.TOTL.GD.ZS?format=jsonP&prefix=jsonp_callback_{1}&per_page=300";

            //worlbank databank: agricultural land in %
            this.WBAGRICULTURALLANDPERCOUNTRYAPI= "http://api.worldbank.org/countries/{0}/indicators/AG.LND.AGRI.ZS?format=jsonP&prefix=jsonp_callback_{1}&per_page=300";

            //worldbank databank: mobile cellular subscriptions per 100 people
            this.WBMOBILESUBSCRIPTIONSPERCOUNTRYAPI = "http://api.worldbank.org/countries/{0}/indicators/IT.CEL.SETS.P2?format=jsonP&prefix=jsonp_callback_{1}&per_page=300";

            //eigen JSON
            this.COUNTRYINFO = "scripts/js/infolanden.js.txt";

            this._dataCountries = null;// Variable for the list of countries
            this._dataCountry = {
                "extrainfo": null,
                "info": null,
                "forrestArea": null,// Variable for the list of forrestArea per year
                "priceDiesel": null, //variabele voor de diesel prijs
                "priceGasoline": null, //variabele voor de gasoline prijs
                "internetConnections": null, //variabele voor het aantal internetconnecties
                "improvedWaterSource": null,	// variabele voor het aantal verbeterde waterbronnen
                "merchandiseTrade": null,	//variabele voor de merchandise handel
                "agriculturalLand": null,	//variabele voor de landbouwgronden
                "mobileSubscriptions": null	//variabele voor de mobiele accounts
            };

            // Handlebars Cache
            this._hbsCache = {};// Handlebars cache for templates
            this._hbsPartialsCache = {};// Handlebars cache for partials

            // Create a clone from the JayWalker object
            this._jayWalker = JayWalker;
            this._jayWalker.init();
            this._jayWalker._countryDetailsJSONPLoaded.add(function(iso2code) {
                self.loadDatasetsFromCountry(iso2code);// Test: load details data from country
            });
            this._jayWalker._countryDetailsDetailsJSONPLoaded.add(function(section, iso2code) {
                switch(section) {
                    case 'forrest':default:
                    self.loadForrestAreaFromCountryFromWorldBankAPI(iso2code);
                    break;

                    case 'pricediesel':
                        self.loadPriceDieselFromCountryFromWorldBankAPI(iso2code);
                        break;

                    case 'pricegasoline':
                        self.loadPriceGasolineFromCountryFromWorldBankAPI(iso2code);
                        break;

                    case 'internetconnections':
                        self.loadInternetConnectionsFromCountryFromWorldBankAPI(iso2code);
                        break;

                    case 'improvedwatersources':
                        self.loadImprovedWaterSourcesFromCountryFromWorldBankAPI(iso2code);
                        break;

                    case 'merchandisetrade':
                        self.loadMerchandiseTradeFromCountryFromWorldBankAPI(iso2code);
                        break;

                    case 'agriculturalland':
                        self.loadAgriculturalLandFromCountryFromWorldBankAPI(iso2code);
                        break;

                    case 'mobilesubscriptions':
                        self.loadMobileSubscriptionsFromCountryFromWorldBankAPI(iso2code);
                        break;
                }
            });

            this.loadCountryInfo();

            this.registerNavigationToggleListeners();// Register All Navigation Toggle Listeners

            this.registerWindowListeners();// Register All Navigation Toggle Listeners


            this.loadCountriesFromWorldBankAPI();// Execute method loadCountriesFromWorldBankAPI(): Load countries from the Worldbank API

        },

        loadCountryInfo: function() {
            // Closure
            var self = this, url = String.format(this.COUNTRYINFO);

            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONByPromise(url).then(
                function(data) {
                    if(data != null) {
                        self._dataCountry.extrainfo = data;
                    }

                },

                function(status) {
                    console.log(status);
                }
            );
        },

        registerNavigationToggleListeners: function() {
            var toggles = document.querySelectorAll('.navigation-toggle');

            if(toggles != null && toggles.length > 0) {
                var toggle = null;

                for(var i = 0; i < toggles.length; i++ ) {
                    toggle = toggles[i];
                    toggle.addEventListener('click', function(ev) {
                        ev.preventDefault();

                        document.querySelector('body').classList.toggle(this.dataset.navtype);

                        return false;
                    });
                }
            }
        },
        registerWindowListeners: function() {
            window.addEventListener('resize', function(ev) {
                if(document.querySelector('body').classList.contains('offcanvas-open')) {
                    document.querySelector('body').classList.remove('offcanvas-open');
                }

                if(document.querySelector('body').classList.contains('headernav-open')) {
                    document.querySelector('body').classList.remove('headernav-open');
                }
            });
        },

        loadCountriesFromWorldBankAPI: function() {
            // Closure
            var self = this, url = String.format(this.WBCOUNTRIESAPIURL, new Date().getTime());

            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONPByPromise(url).then(
                function(data) {
                    if(data != null) {
                        var countries = data[1]; // Get the countries from JSON (second item from array, first item is paging)
                        var countriesFiltered = _.filter(countries, function(country) {
                            return !(/\d/.test(country.iso2Code));
                        });// First remove weird countries with LoDash + Assign data as value flor global variable _dataCountries within the App
                        var badISO2Codes = ['XT', 'XN', 'ZG', 'ZF', 'OE', 'XS', 'XR', 'XU', 'XQ', 'XP', 'ZQ', 'XO', 'XN', 'XM', 'XL', 'ZJ', 'XJ', 'XY', 'XE', 'EU', 'XC', 'JG', 'XD'];
                        self._dataCountries = _.filter(countriesFiltered, function(country) {
                            var validCountry = true, i = 0;

                            while(validCountry && i < badISO2Codes.length) {
                                if(country.iso2Code == badISO2Codes[i]) {
                                    validCountry = false;
                                } else {
                                    i++;
                                }
                            }

                            return validCountry;
                        });// Filter (weird codes: XT, XN, ZG, ZF, OE, XS, XR, XU, XQ, XP, ZQ, XO, XN, XM, XL, ZJ, XJ, XY, XE, EU, XC, JG)
                        self._dataCountries = _.sortBy(self._dataCountries, function(country) {
                            return country.name;
                        });// Sorting on country name
                        self.updateCountriesUI('countries-list', '#countries-list-template');// Call updateCountriesUI method when successful*/
                    }
                },
                function(status) {
                    console.log(status);
                }
            );
        },
        updateCountriesUI: function(hbsTmplName, hbsTmplId) {
            if(!this._hbsCache[hbsTmplName]) {
                var src = document.querySelector(hbsTmplId).innerHTML;// Get the contents from the specified hbs template
                this._hbsCache[hbsTmplName] = Handlebars.compile(src);// Compile the source and add it to the hbs cache
            }
            document.querySelector('.countries-list').innerHTML = this._hbsCache[hbsTmplName](this._dataCountries);// Write compiled content to the appropriate container
        },


        loadDatasetsFromCountry: function(iso2code) {
            console.log(this._dataCountries);
            var selectedCountry = _.find(this._dataCountries, function(country) {
                return country.iso2Code == iso2code;
            });
            if(selectedCountry != null && typeof selectedCountry != undefined) {
                this._dataCountry.info = selectedCountry;
            }

            if(!this._hbsCache['country-details']) {
                var src = document.querySelector('#country-details-template').innerHTML;// Get the contents from the specified hbs template
                this._hbsCache['country-details'] = Handlebars.compile(src);// Compile the source and add it to the hbs cache
            }
            document.querySelector('.country-details').innerHTML = this._hbsCache['country-details'](selectedCountry);// Write compiled content to the appropriate container
            console.log(selectedCountry);
        },
        loadForrestAreaFromCountryFromWorldBankAPI: function(iso2code) {
            // Closure
            var self = this, url = String.format(this.WBFORRESTAREAPERCOUNTRYAPI, iso2code, new Date().getTime() + Math.round(Math.random()*10));

            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONPByPromise(url).then(
                function(data) {
                    if(data != null) {
                        var forrestArea = data[1]; // Get the forrest area from the selected country from JSON (second item from array, first item is paging)
                        var forrestAreaFiltered = _.filter(forrestArea, function(forrestAreaPerYear) {
                            return forrestAreaPerYear.value != null;
                        });// First remove all years where value is null with LoDash
                        forrestAreaFiltered = _.sortBy(forrestAreaFiltered, function(forrestAreaPerYear) {
                            return forrestAreaPerYear.year;
                        });// Sorting on year
                        self._dataCountry.forrestArea = forrestAreaFiltered;// Add the forrest area data to the details of a country
                        self.updateCountryDetailsUI('country-details-forrest', '#countries-details-forrest-template', '#countries_details_forrest .country-details');// Call updateCountryDetailsUI method when successful
                    }

                },

                function(status) {
                    console.log(status);
                }
            );
        },

        loadExtraInfoDetails: function(iso2code) {
            // Closure
            var self = this, url = String.format(this.EXTRAINFOABOUTDETAILS, iso2code, new Date().getTime() + Math.round(Math.random()*10));


            Utils.getJSONPByPromise(url).then(
                function(data) {
                    if(data != null) {
                        var infoDetails = data[0]; // Get the forrest area from the selected country from JSON (second item from array, first item is paging)
                        self._dataCountry.infoDetails = infoDetails;// Add the forrest area data to the details of a country
                        self.updateCountryDetailsUI('country-details-forrest', '#countries-details-forrest-template', '#countries_details_forrest .country-details');// Call updateCountryDetailsUI method when successful
                    }
                },
                function(status) {
                    console.log(status);
                }
            );
        },

        loadPriceDieselFromCountryFromWorldBankAPI: function(iso2code) {

            //closure
            var self = this, url = String.format(this.WBPRICEDIESELPERCOUNTRYAPI, iso2code, new Date().getTime() + Math.round(Math.random()*10));

            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONPByPromise(url).then(
                function(data) {

                    if(data != null) {
                        var priceDiesel = data[1]; // Get the diesel price from the selected country from JSON (second item from array, third item is paging)
                        var priceDieselFiltered = _.filter(priceDiesel, function(price) {
                            return price.value != null && price.value > 0;
                        });// First remove all years where value is null with LoDash
                        priceDieselFiltered = _.sortBy(priceDieselFiltered, function(price) {
                            return price.date;
                        });// Sorting on date

                        self._dataCountry.priceDiesel = priceDieselFiltered;// Add the price of diesel data to the details of a country
                        self.updateCountryDetailsUI('country-details-pricediesel', '#countries-details-pricediesel-template', '#countries_details_pricediesel .country-details');// Call updateCountryDetailsUI method when successful
                    }
                },
                function(status) {
                    console.log(status);
                }
            );

        },

        loadPriceGasolineFromCountryFromWorldBankAPI: function(iso2code){

            //closure
            var self = this, url = String.format(this.WBPRICEGASOLINEPERCOUNTRYAPI, iso2code, new Date().getTime() + Math.round(Math.random()*10));

            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONPByPromise(url).then(
                function(data) {
                    if(data != null) {
                        var priceGasoline = data[1]; // Get the gasoline price from the selected country from JSON (second item from array, third item is paging)
                        var priceGasolineFiltered = _.filter(priceGasoline, function(price) {
                            return price.value != null && price.value > 0;
                        });// First remove all years where value is null with LoDash
                        priceGasolineFiltered = _.sortBy(priceGasolineFiltered, function(price) {
                            return price.date;
                        });// Sorting on date
                        self._dataCountry.priceGasoline = priceGasolineFiltered;// Add the price of gasoline data to the details of a country
                        self.updateCountryDetailsUI('country-details-pricegasoline', '#countries-details-pricegasoline-template', '#countries_details_pricegasoline .country-details');// Call updateCountryDetailsUI method when successful
                    }
                },
                function(status) {
                    console.log(status);
                }
            );


        },

        loadInternetConnectionsFromCountryFromWorldBankAPI: function(iso2code){

            //closure
            var self = this, url = String.format(this.WBINTERNETCONNECTIONSPERCOUNTRYAPI, iso2code, new Date().getTime() + Math.round(Math.random()*10));

            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONPByPromise(url).then(
                function(data) {
                    if(data != null) {
                        var internetConnections = data[1]; // Get the internetconnections from the selected country from JSON (second item from array, third item is paging)
                        var internetConnectionsFiltered = _.filter(internetConnections, function(price) {
                            return price.value != null && price.value > 0;
                        });// First remove all years where value is null with LoDash
                        internetConnectionsFiltered = _.sortBy(internetConnectionsFiltered, function(price) {
                            return price.date;
                        });// Sorting on date
                        self._dataCountry.internetConnections = internetConnectionsFiltered;// Add the internetconnections data to the details of a country
                        self.updateCountryDetailsUI('country-details-internetconnections', '#countries-details-internetconnections-template', '#countries_details_internetconnections .country-details');// Call updateCountryDetailsUI method when successful
                    }
                },
                function(status) {
                    console.log(status);
                }
            );


        },

        loadImprovedWaterSourcesFromCountryFromWorldBankAPI: function(iso2code){

            //closure
            var self = this, url = String.format(this.WBIMPROVEDWATERSOURCESPERCOUNTRYAPI, iso2code, new Date().getTime() + Math.round(Math.random()*10));

            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONPByPromise(url).then(
                function(data) {
                    if(data != null) {
                        var improvedWaterSources = data[1]; // Get the improved water source from the selected country from JSON (second item from array, third item is paging)
                        var improvedWaterSourcesFiltered = _.filter(improvedWaterSources, function(price) {
                            return price.value != null && price.value > 0;
                        });// First remove all years where value is null with LoDash
                        improvedWaterSourcesFiltered = _.sortBy( improvedWaterSourcesFiltered, function(price) {
                            return price.date;
                        });// Sorting on date
                        self._dataCountry.improvedWaterSources =  improvedWaterSourcesFiltered;// Add the improved water source // data to the details of a country
                        self.updateCountryDetailsUI('country-details-improvedwatersources', '#countries-details-improvedwatersources-template', '#countries_details_improvedwatersources .country-details');// Call updateCountryDetailsUI method when successful
                    }
                },
                function(status) {
                    console.log(status);
                }
            );


        },
        loadMerchandiseTradeFromCountryFromWorldBankAPI: function(iso2code){

            //closure
            var self = this, url = String.format(this.WBMERCHANDISETRADEPERCOUNTRYAPI, iso2code, new Date().getTime() + Math.round(Math.random()*10));

            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONPByPromise(url).then(
                function(data) {
                    if(data != null) {
                        var merchandiseTrade = data[1]; // Get the merchandise trade from the selected country from JSON (second item from array, third item is paging)
                        var merchandiseTradeFiltered = _.filter(merchandiseTrade, function(price) {
                            return price.value != null && price.value > 0;
                        });// First remove all years where value is null with LoDash
                        merchandiseTradeFiltered = _.sortBy( merchandiseTradeFiltered, function(price) {
                            return price.date;
                        });// Sorting on date
                        self._dataCountry.merchandiseTrade =  merchandiseTradeFiltered;// Add the merchandise trade data to the details of a country
                        self.updateCountryDetailsUI('country-details-merchandisetrade', '#countries-details-merchandisetrade-template', '#countries_details_merchandisetrade .country-details');// Call updateCountryDetailsUI method when successful
                    }
                },
                function(status) {
                    console.log(status);
                }
            );


        },

        loadAgriculturalLandFromCountryFromWorldBankAPI: function(iso2code){

            //closure
            var self = this, url = String.format(this.WBAGRICULTURALLANDPERCOUNTRYAPI, iso2code, new Date().getTime() + Math.round(Math.random()*10));

            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONPByPromise(url).then(
                function(data) {
                    if(data != null) {
                        var agriculturalLand = data[1]; // Get the agricultural land from the selected country from JSON (second item from array, third item is paging)
                        var agriculturalLandFiltered = _.filter(agriculturalLand, function(price) {
                            return price.value != null && price.value > 0;
                        });// First remove all years where value is null with LoDash
                        agriculturalLandFiltered = _.sortBy( agriculturalLandFiltered, function(price) {
                            return price.date;
                        });// Sorting on date
                        self._dataCountry.agriculturalLand =  agriculturalLandFiltered;// Add the agricultural land data to the details of a country
                        self.updateCountryDetailsUI('country-details-agriculturalland', '#countries-details-agriculturalland-template', '#countries_details_agriculturalland .country-details');// Call updateCountryDetailsUI method when successful
                    }
                },
                function(status) {
                    console.log(status);
                }
            );


        },

        loadMobileSubscriptionsFromCountryFromWorldBankAPI: function(iso2code){

            //closure
            var self = this, url = String.format(this.WBMOBILESUBSCRIPTIONSPERCOUNTRYAPI, iso2code, new Date().getTime() + Math.round(Math.random()*10));

            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONPByPromise(url).then(
                function(data) {
                    if(data != null) {
                        var mobileSubscriptions = data[1]; // Get the agricultural land from the selected country from JSON (second item from array, third item is paging)
                        var mobileSubscriptionsFiltered = _.filter(mobileSubscriptions, function(price) {
                            return price.value != null && price.value > 0;
                        });// First remove all years where value is null with LoDash
                        mobileSubscriptionsFiltered = _.sortBy( mobileSubscriptionsFiltered, function(price) {
                            return price.date;
                        });// Sorting on date
                        self._dataCountry.mobileSubscriptions =  mobileSubscriptionsFiltered;// Add the agricultural land data to the details of a country
                        self.updateCountryDetailsUI('country-details-mobilesubscriptions', '#countries-details-mobilesubscriptions-template', '#countries_details_mobilesubscriptions .country-details');// Call updateCountryDetailsUI method when successful
                    }
                },
                function(status) {
                    console.log(status);
                }
            );


        },


        updateCountryDetailsUI: function(hbsTmplName, hbsTmplId, container) {
            if(!this._hbsCache[hbsTmplName]) {
                var src = document.querySelector(hbsTmplId).innerHTML;// Get the contents from the specified hbs template
                this._hbsCache[hbsTmplName] = Handlebars.compile(src);// Compile the source and add it to the hbs cache
            }
            document.querySelector(container).innerHTML = this._hbsCache[hbsTmplName](this._dataCountry);// Write compiled content to the appropriate container
            this.createForrestAreaGraphForCountry();
            this.createPriceDieselGraphForCountry();
            this.createPriceGasolineGraphForCountry();
            this.createInternetConnectionsGraphForCountry();
            this.createImprovedWaterSourcesGraphForCountry();
            this.createMerchandiseTradeGraphForCountry();
            this.createAgriculturalLandGraphForCountry();
            this.createMobileSubscriptionsGraphForCountry();

        },
        createForrestAreaGraphForCountry: function() {
            if(this._dataCountry.forrestArea != null) {
                var labels = [], series = [];
                _.each(this._dataCountry.forrestArea.reverse(), function(item) {
                    labels.push(item.date);
                    series.push(parseFloat(item.value));
                });

                var options = {
                    low: _.min(_.pluck(this._dataCountry.forrestArea, 'value')),
                    hight: _.max(_.pluck(this._dataCountry.forrestArea, 'value'))
                };

                var data = {
                    labels: labels,
                    series: [series]
                };
                // Create a new line chart object where as first parameter we pass in a selector that is resolving to our chart container element. The Second parameter is the actual data object.
                new Chartist.Line('.country-details-forrestarea-chart', data, options);
            }
        },

        createPriceDieselGraphForCountry: function() {
            if(this._dataCountry.priceDiesel != null) {
                var labels = [], series = [];
                _.each(this._dataCountry.priceDiesel.reverse(), function(item) {
                    labels.push(item.date);
                    series.push(parseFloat(item.value));
                });

                var options = {
                    low: _.min(_.pluck(this._dataCountry.priceDiesel, 'value')),
                    hight: _.max(_.pluck(this._dataCountry.priceDiesel, 'value'))
                };

                var data = {
                    labels: labels,
                    series: [series]
                };
                // Create a new line chart object where as first parameter we pass in a selector that is resolving to our chart container element. The Second parameter is the actual data object.
                new Chartist.Line('.country-details-pricediesel-chart', data, options);
            }
        },

        createPriceGasolineGraphForCountry: function() {
            if(this._dataCountry.priceGasoline != null) {
                var labels = [], series = [];
                _.each(this._dataCountry.priceGasoline.reverse(), function(item) {
                    labels.push(item.date);
                    series.push(parseFloat(item.value));
                });

                var options = {
                    low: _.min(_.pluck(this._dataCountry.priceGasoline, 'value')),
                    hight: _.max(_.pluck(this._dataCountry.priceGasoline, 'value'))
                };

                var data = {
                    labels: labels,
                    series: [series]
                };
                // Create a new line chart object where as first parameter we pass in a selector that is resolving to our chart container element. The Second parameter is the actual data object.
                new Chartist.Line('.country-details-pricegasoline-chart', data, options);
            }
        },

        createInternetConnectionsGraphForCountry: function() {
            if(this._dataCountry.internetConnections != null) {
                var labels = [], series = [];
                _.each(this._dataCountry.internetConnections.reverse(), function(item) {
                    labels.push(item.date);
                    series.push(parseFloat(item.value));
                });

                var options = {
                    low: _.min(_.pluck(this._dataCountry.internetConnections, 'value')),
                    hight: _.max(_.pluck(this._dataCountry.internetConnections, 'value'))
                };

                var data = {
                    labels: labels,
                    series: [series]
                };
                // Create a new line chart object where as first parameter we pass in a selector that is resolving to our chart container element. The Second parameter is the actual data object.
                new Chartist.Line('.country-details-internetconnections-chart', data, options);
            }
        },

        createImprovedWaterSourcesGraphForCountry: function() {
            if(this._dataCountry.improvedWaterSources != null) {
                var labels = [], series = [];
                _.each(this._dataCountry.improvedWaterSources.reverse(), function(item) {
                    labels.push(item.date);
                    series.push(parseFloat(item.value));
                });

                var options = {
                    low: _.min(_.pluck(this._dataCountry.improvedWaterSources, 'value')),
                    hight: _.max(_.pluck(this._dataCountry.improvedWaterSources, 'value'))
                };

                var data = {
                    labels: labels,
                    series: [series]
                };
                // Create a new line chart object where as first parameter we pass in a selector that is resolving to our chart container element. The Second parameter is the actual data object.
                new Chartist.Line('.country-details-improvedwatersources-chart', data, options);
            }
        },

        createMerchandiseTradeGraphForCountry: function() {
            if(this._dataCountry.merchandiseTrade != null) {
                var labels = [], series = [];
                _.each(this._dataCountry.merchandiseTrade.reverse(), function(item) {
                    labels.push(item.date);
                    series.push(parseFloat(item.value));
                });

                var options = {
                    low: _.min(_.pluck(this._dataCountry.merchandiseTrade, 'value')),
                    hight: _.max(_.pluck(this._dataCountry.merchandiseTrade, 'value'))
                };

                var data = {
                    labels: labels,
                    series: [series]
                };
                // Create a new line chart object where as first parameter we pass in a selector that is resolving to our chart container element. The Second parameter is the actual data object.
                new Chartist.Line('.country-details-merchandisetrade-chart', data, options);
            }
        },

        createAgriculturalLandGraphForCountry: function() {
            if(this._dataCountry.agriculturalLand != null) {
                var labels = [], series = [];
                _.each(this._dataCountry.agriculturalLand.reverse(), function(item) {
                    labels.push(item.date);
                    series.push(parseFloat(item.value));
                });

                var options = {
                    low: _.min(_.pluck(this._dataCountry.agriculturalLand, 'value')),
                    hight: _.max(_.pluck(this._dataCountry.agriculturalLand, 'value'))
                };

                var data = {
                    labels: labels,
                    series: [series]
                };
                // Create a new line chart object where as first parameter we pass in a selector that is resolving to our chart container element. The Second parameter is the actual data object.
                new Chartist.Line('.country-details-agriculturalland-chart', data, options);
            }
        },

        createMobileSubscriptionsGraphForCountry: function() {
            if(this._dataCountry.mobileSubscriptions != null) {
                var labels = [], series = [];
                _.each(this._dataCountry.mobileSubscriptions.reverse(), function(item) {
                    labels.push(item.date);
                    series.push(parseFloat(item.value));
                });

                var options = {
                    low: _.min(_.pluck(this._dataCountry.mobileSubscriptions, 'value')),
                    hight: _.max(_.pluck(this._dataCountry.mobileSubscriptions, 'value'))
                };

                var data = {
                    labels: labels,
                    series: [series]
                };
                // Create a new line chart object where as first parameter we pass in a selector that is resolving to our chart container element. The Second parameter is the actual data object.
                new Chartist.Line('.country-details-mobilesubscriptions-chart', data, options);
            }
        }
    };

    App.init();// Intialize the application

    //functie voor de zoekbar
    document.querySelector('#txtSearch').addEventListener('keyup', function(ev)           {
        ev.preventDefault();

        var txt = ev.target.value.toLowerCase();

        var countryNodes = document.querySelectorAll('.countries-list>ul>li');
        for(var i=0;i<countryNodes.length;i++) {
            var c = countryNodes[i];
            if(c.dataset.title.toLowerCase().indexOf(txt) == -1) {
                c.classList.add('countryHidden');
            } else {
                c.classList.remove('countryHidden');
            }
        }

        return false;
    });

})();